﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketsCenter.Models.Tickets
{
    public class Tickets
    {
        public int TicketNumber { get; set; }
        public DateTime SubmitDate { get; set; }
        public DateTime AssignedDate { get; set; }
        public DateTime CompletedDate { get; set; }
        public int Progress { get; set; }
        public int UserId { get; set; }
    }
}