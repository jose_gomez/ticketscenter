﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketsCenter.Models
{
    public class Users
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime CreateDate { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime LastLoggedOn { get; set; }
        public string Password { get; set; }
    }
}