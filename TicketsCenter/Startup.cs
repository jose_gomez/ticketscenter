﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TicketsCenter.Startup))]
namespace TicketsCenter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
